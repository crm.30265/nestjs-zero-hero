import {
  Controller,
  Get,
  Post,
  Delete,
  Patch,
  Body,
  Param,
  Query,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TasksModel } from './tasks.model';

import { CreateTasksDto } from './dto/create-tasks.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';

@Controller('tasks')
export class TasksController {
  constructor(private tasksService: TasksService) {
    this.tasksService = tasksService;
  }
  @Get()
  getAllTasks(@Query() filterDto: GetTasksFilterDto): TasksModel[] {
    if (Object.keys(filterDto).length) {
      return this.tasksService.getTasksWithFilters(filterDto);
    }

    return this.tasksService.getAllTasks();
  }
  @Post()
  createTask(@Body() createTasksDto: CreateTasksDto): TasksModel {
    return this.tasksService.createTask(createTasksDto);
  }
  @Get('/:id')
  getTaskById(@Param('id') id: string): TasksModel {
    return this.tasksService.getTaskById(id);
  }
  @Patch('/:id/status')
  updateTasksStatus(
    @Param('id') id: string,
    @Body() updateTaskStatusDto: UpdateTaskStatusDto,
  ): TasksModel {
    const { status } = updateTaskStatusDto;
    return this.tasksService.updateTasksStatus(id, status);
  }
  @Delete('/:id')
  deleteTaskById(@Param('id') id: string): {
    message: string;
    id: string;
  } {
    return this.tasksService.deleteTaskById(id);
  }
}
